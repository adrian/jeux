\- 72 cartes dont :  
\- 20 cartes Action (5 araignées, 5 moustiques, 5 cafards, 5 fourmis)  
\- 43 cartes Nombre (8x1, 8x2, 9x3, 9x4, 9x5)
\- 8 cartes Mito (2x1, 2x2, 2x3, 1x4, 1x5)  
\- 1 gardienne punaise  
\- 1 livret de règles Mito

## Mito, la règle du jeu expliquée

Dans Mito, débarrassez-vous de vos cartes en main le plus rapidement possible. Tous les moyens sont bons pour arriver à vos fins même celle qui est bannie dans tous les autres jeux, j'appelle la triche ! Pour se débarrasser de vos cartes soyez honnête et suivez le cours du jeu ou bien trichez et dissimuler des cartes dans votre manche, sous votre chaise mais sans se faire prendre par la Gardienne Punaise!

### Les règles de Mito

**Principe de Mito**  
Les cartes possèdent un nombre. Lorsque vous en jouez une, le nombre de votre carte doit être directement inférieur ou supérieur au nombre de la carte sur la table. Les nombres vont jusque 5. Il existe toutefois des exceptions : une carte 5 peut être recouverte par une carte 4 ou 1. De la même façon, une carte 1 peut être recouverte par une carte 5 ou 2.

**La Gardienne Punaise**  
Chacun son tour, au cours du jeu, un joueur prends le rôle de la Gardienne Punaise. La Gardienne Punaise veille sur les autres joueurs afin d'être sûr qu'ils respectent les règles. Lorsque vous êtes la Gardienne Punaise vous devez restez honnête.  
Si la Gardienne Punaise a un soupçon de triche, elle dit « Vu ! », le joueur concerné est obligé de dire la vérité sur oui ou non il était en train de tricher. Si la Gardienne Punaise a raison, le joueur reprend la carte qu'il a tenté de dissimuler et une carte de la main de la Gardienne Punaise. De plus, il devient la nouvelle Gardienne Punaise. Si la Gardienne Punaise a tord, elle pioche une carte.

**La carte Mito**  
La carte Mito ne peut pas être posée sur la table ni offerte à un adversaire, vous devez la faire disparaître.

**La carte Action araignée**  
Un joueur qui joue une araignée peut offrir une carte de son choix à un autre joueur (sauf la carte Mito bien évidemment).

**La carte Action moustique**  
Lorsqu'un moustique est joué, tous les joueurs (hormi le poseur de carte) doivent taper sur la carte. Le joueur le plus lent reçoit une carte de celui qui vient de poser le moustique.

**La carte Action cafard**  
Si un joueur joue une carte cafard, les autres joueurs doivent poser une carte de la même valeur le plus rapidement possible. Seul le plus rapide laisse sa carte sur la pile.

**La carte Action fourmi**  
Lorsqu'une carte fourmi est jouée, tous les joueurs piochent.

**Fin de manche**  
Dès qu’un joueur n’a plus de carte en main, la manche s’arrête. En ce qui concerne le décompte de la manche:  
\- Chaque carte Nombre encore en main fait perdre 1 point  
\- Chaque carte Action, 5 points  
\- Chaque carte Mito, 10 points  
Le but est de ne plus avoir de cartes dans sa main en fin de manche et à défaut d'en avoir un minimum en main!

Mito, enfin un jeu où il vous est permis de tricher ! Fous rires garantis!
