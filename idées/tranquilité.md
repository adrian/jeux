80 cartes numérotées de 1 à 80

Coopératif mais en silence

on en a 36

On doit les disposer par ordre croissant

Préparation

* On mélange les 80 cartes
* distribue 5 à chaque joueurs
* on délimite en carré de 6x6 qui compose un chemin de 36 tuiles et on dit où sera le début et la fin

Tour

* Si iel a une carte départ et qu'aucune n'a été posée jusque là iel est obligé.e de la placer au depart de la suite et les joueureuses vont devoir défausser collectivement 8 cartes (iels peuvent alors communiquer).
* 
* un.e joueureuse pose une de ses carte où iel le souhaite
* si la carte est adjacente à au moins une carte, on regarde la plus proche, on calcule la différence et on défausse autant de cartes (les cartes au dessus ou en dessous ne sont pas adjacentes, il ne peut y en avoir que 0, 1 ou 2. Les cartes d'un côté à l'autre du plateau à une ligne près sont une suite et sont donc adjacentes)
* On pioche pour compléter sa main

Condition de victoire

* Si la grille est pleine un.e joueureuse peut placer la carte arrivée à la fin de la suite et la partie est gagnée

Conditions de défaite

* Si un.e joueureuse ne peux plus jouer ou piocher de carte la partie est perdue