# Concept

* 2 à 12 joueureuses
* 10 minutes par tour, autant de tours que vous voulez
* à partir de 8 ans (c'est ce qu'il y a sur la boîte, ça vaut ce que ça vaut)

## Matériel

* Le plateau disponible ici <https://pixeladventurers.com/wp-content/uploads/2018/07/concept-12-1024x1024.jpg>
* L'aide pour être d'accord sur la signification des icones
* des mots à faire deviner (il y a des cartes officielles ou l'imagination ça va très bien)
* 1 pion concept (gros) et 10 petits jetons de la même couleur
* 4 pions sous-concept (d'une couleur chacuns) et 8 petits jetons par sousconcept (de la même couleur que leur concept)

## Déroulement d'un tour

1. Choix d'un.e joueureuse qui devra faire deviner (à tour de rôle ça fonctionne bien).
2. Choix d'un mot à faire deviner (piocher dans un chapeau ou dans sa tête).
3. Faire deviner ce mot en plaçant les pions et les jetons  
   \- Montrer quel est le concept principal et le qualifier avec les cubes.  
   \- Montrer des sous-concepts liés et les qualifier avec des cubes  
   *On peut limiter le temps si on veut*

## Fin d'un tour

Quand quelqu'un.e a deviné le mot ou que l'on abandonne parce que c'est trop dur :'( 

## Exemples de mots

liste de mots par des fans :   
<https://docs.google.com/spreadsheets/d/14sCZxhuBu-1z7cOsOz--Y-mkVrRRkxnBdBAjwv10TYg/edit#gid=0>

| Concept | remarque | difficulté 1 à 3 |
|---------|----------|------------------|
| La seconde guerre mondiale |  | 1 |
| Bordeaux | ville française | 1 |
| athédrale | église ou siège de l'éveque | 2 |
| Démineurs |  | 2 |
| Faire ses valises |  | 2 |
| FBI |  | 2 |
| internet |  | 2 |
| La finale de roland garros |  | 2 |
| La pêche à la mouche |  | 2 |
| Le cheval de troie |  | 2 |
| Le loup de wall street |  | 2 |
| Led zepplin |  | 2 |
| racine carrée de 3 |  | 2 |
| Déboulonner |  | 3 |
| l'alphabet |  | 3 |
| Le monopoly |  | 3 |
| Les lépidoptères | famille des papillons | 3 |
| Pumba | personnage dans le roi lion | 3 |
| Tirer à la courte paille |  | 3 |
