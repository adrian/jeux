# The crew

## Matériel
- 1 jeton par personne (pour communiquer sur ses cartes. Des bouts de papier font l’affaire)
- Les jetons 1, 2, 3, 4, Ω, <, <<, <<< (Des bouts de papier font l’affaire)
- Les atouts de 1 à 4 d’un jeu de Tarot
- Les cartes de 1 à 9 d’un jeu de Tarot
- Les cartes de 1 à 9 d’un jeu de 54 cartes

Les cartes de Tarot seront utilisées pour jouer, les petites seront utilisées pour choisir les quêtes.

## Règles
Se référer aux règles dans le dossier 'The Crew'
