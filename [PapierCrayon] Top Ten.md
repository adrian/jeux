# Top Ten

## Materiel
* Un papier et un crayon par joueureuse
* Des jetons secrets avec des numéros de 1 à 10 compris (on peut utiliser des cartes à jouer)

# Apperçu

Top Ten est un jeu coppératif et drôle. 

Le but du jeu est que les joueurs répondent tous à l’un des thèmes en fonction de leur numéro (entre 1 et 10). Ensuite, l’un d'entre eux tente de remettre toutes les réponses dans le bon ordre pour faire gagner l’équipe !

 

## Étapes de jeu : 

1 - Lire un thème. Exemple : *Quel objet fallait-il avoir lors du naufrage du Titanic ? du **moins utile** au **plus utile**.*

2 - Chaque joueur invente et annonce une réponse correspondant à son n° secret de **1** (le moins utile) à **10** (le plus utile) :

*“Une bouée canard” ; “Un jet-ski” ; “Une machine à remonter le temps” ; “Un appareil à raclette”…*

3 - Si le ***Cap’ten*** arrive à remettre les réponses dans le bon l’ordre, du plus petit au plus grand n°, l’équipe a gagné !
