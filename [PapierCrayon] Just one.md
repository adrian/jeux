# Just One

* 3 à 7 joueureuses
* 5 minutes par tour, autant de tours que vous voulez
* à partir de 8 ans (c'est ce qu'il y a sur la boîte, ça vaut ce que ça vaut)

Faire deviner des mots avec des indices... Mais attention à ne pas être trop évident !

## Matériel

* Un papier et un stylo (ou feutre et ardoise) par joueureuse (sauf un.e).

## Déroulement d'un tour

1. Choix d'un.e joueureuse qui devra deviner (à tour de rôle ça fonctionne bien).
2. La sélection du mot Mystère : les autres joueureuses choisissent un mot (débrouillez vous).
3. Le choix des indices : les autres joueureuses écrivent un mot sur leur papier pour aider lae devinant.e à trouver le mot. Les joueureuses n'ont pas le droit de se concerter.
4. La comparaison des indices : les autres joueureuses comparent leurs mots et éliminent ceux qui sont identiques !
5. La réponse : lae devinant.e essaye de trouver le mot Mystère avec les indices restants.

### 