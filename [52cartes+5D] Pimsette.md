# Pimsette

## Matériel
- 5 dés
- 52 cartes

## Préparation
- Mélanger les cartes et les poser sur le côté : ce sera la pioche

## Déroulé d’une partie
Chaqun·e à son tour, les joueureuses vont faire leur manche.
- Lancer 5 dés.
- Dire « pims » pour que le dealer retourne une carte (on peut dire pims autant qu’on veut).
- Il faut faire correspondre les dés avec les valeurs des cartes ( par exemple les dés 3 et 2 vont aller sur la carte 5)
  (L’As vaut 1 ou 10 au choix, le valet vaut 1 dés de n’importe quelle valeur, la dame vaut 2 dés et le roi 3).
- Si les dés correspondent pile aux valeurs des cartes, ces dernières sont remportées par lae joueureuse.
- Si les dés ne permettent pas de tomber pile, la manche est perdue.
- Si tous les dés sont placés on peut rejouer
- Coup spécial inutile : le pims flush ; avoir un dé par carte.

- Avant un pims, les auters joueureuses peuvent faire un contre pims.
- IEles peuvent miser entre 1 et N cartes (au tour N) pour inciter l’autre à pimser.
- Si le pims réussi, les cartes misées sont empilées sur la carte pimsée, elles peuvent être gagnées par lae joueureuse en cours.
- Si le pims échoue, les contre-pimseurs doublent leur mise de carte. Ces dernières ne vont pas dans leur pile de carte mais en hypothèque. Elles ne seront gagnées que si leur prochain pims est réussi.
- Si le pims réussi mais que lae joueureuse ne s’arrête pas et échoue un pims, toutes les cartes vont à la défausse.

- Chaque carte vaut 1 point à la fin de la partie
- Variante : on peut autoriser de ne pas pimser à son tour pour récupérer son hypothèque.
