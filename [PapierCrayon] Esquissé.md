# Esquissé ?

* 4 à 8 joueureuses
* 10 à 15 minutes par tour, autant de tours que vous voulez
* à partir de 8 ans (c'est ce qu'il y a sur la boîte, ça vaut ce que ça vaut)

## Matériel

* Un grand papier (A4) et un stylo (ou le calpin d'ardoises officiel) par joueureuse.

## Préparation

* Chacun.e prend une feuille vide
* Chacun.e choisi un mot (on se met d'accord sur la difficulté)
* Chacun.e écrit le mot en haut de la feuille et la passe à la personne suivante

## Déroulement d'un tour

1. Chaque joueureuse reçoit une feuille de la personne précédente et attend que tout le monde ait bien sa feuille pour regarder la sienne.
2. Si iel reçoit un mot, iel doit le dessiner au dessous.  
   Si iel reçoit un dessin, iel doit retrouver le mot correspondant.  
   *On peut limiter le temps si on le souhaite.*
3. Chacun.e plie la feuille pour ne laisser visible que la dernière production et passe la feuille à la personne suivante.

## Fin de la partie

Quand chacun.e a retrouvé sa feuille (les papiers ont fait un tour complet) on les déplie pour admirer les déformations successives qu'a subit notre mot :)