# Longueur d’onde

* 2 à N joueureuses
* 5 minutes par tour (ou plus si les gens hésitent), autant de tours que vous voulez

## Matériel

* Un générateur de chiffres aléatoire (un dé avec au moins 12 faces, le cadran en plastique dans le jeu original, un site web qui tire des nombres au hazard…)
* Des cartes avec différentes échelles de valeurs (de frois à chaud, intelligent à stupide, petit à minuscule…) vous pouvez inventer les vôtres ou prendre les originales dans le jeu
TODO : récupérer la liste originale des mots ?

## Préparation

* Préparer le générateur et les cartes

## Déroulement d'un tour

* On désigne lae joueureuse principale
* Iel tire un nombre aléatoire et une échelle de valeur (exemple : le nombre 2 pris entre 0 et 20, et l’échelle de valeur de « bon pour la santé » à « mauvais pour la santé »).
* Iel choisi un indice. C’est un mot, une phrase, un nom, une idée ou n’importe quoi qui serait à 12 sur l’échelle de bon à mauvais pour la santé (par exemple « du cyanure pas assez dillué »)
* Iel dit son indice et l’échelle aux autres joueureuses qui doivent retrouver le nombre.



## Fin du tour

* Quand les autres joueureuses ont choisi quel nombre proposer à lae joueureuse principale

## Version compétitive ou calcul des scores

* On peut définir des équipes, et le jeu se joue à l’interieur de chaque équipe à tour de rôle (un peu comme au time’s up)
* On peut définir des points gagnés à chaque manche comme la distance entre le chiffre à deviner et le chiffre proposé. Le but est alors d’avoir le moins de points possibles (version difficile).
* On peut caper le nombre de points à 4 pour ne pas perdre trop de point d’un coup sur une erreur (version plus facile).

