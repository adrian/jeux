# The crew

## Matériel
- 1 jeton par personne (pour communiquer sur ses cartes. Des bouts de papier font l’affaire)
- Les atouts de 1 à 4 d’un jeu de Tarot
- Les cartes de 1 à 9 d’un jeu de Tarot

Les cartes de Tarot seront utilisées pour jouer, les cartes de quêtes sont à générer avec la page web dans le dossier 'The Crew'.

## Règles
Se référer aux règles dans le dossier 'The Crew'
