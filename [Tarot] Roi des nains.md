# Le Roi des Nains (adapté)

## Notes d’adaptation
```
Nains : Trèfle
Chevaliers : Pique
Gobelins : Carreau
Est-ce qu’on cherche à ajouter des têtes ? Il y en a 4 dans le jeu original (vdra). On peut garder celles du tarot. Mais du coup ce sont les 10 les cartes spéciales 11 ?
```

## Matériel
Un jeu de tarot (+6 cartes 11 si possibles)
Il est recommandé de modifier le jeu pour y inscrire les effets des cartes.

- 36 (ou 39) Cartes de 2 à roi (sauf les valets et cavaliers).
- 14 cartes spéciales (trois 1, six cavaliers et valets, cinq cartes magiques)
- 20 quêtes

### Cartes spéciales
Les 5 cartes d’Atout ne remportent rien au compte des points.
- 1 Atout : Atout. Vous ne pouvez jouer cette carte que si vous n’avez plus de carte de la couleur demandée. Vous remportez le pli. Si c’est la derinère carte de votre main et que vous devez ouvrir le pli avec, la couleur du pli est déterminée par la carte du joueur suivant et vous ne remportez pas le pli.
- 2 Atout : Sorcier. Jouez cette carte entre deux plis. Choisissez une carte du dernier pli joué et remplacez-la par le sorcier. Récupérez la carte choisie dans votre main.
- 3 Atout : Dragon. Vous pouvez toujouns jouer le Dragon, même si vous avez des cartes de la couleur demandée. Le Dragon remporte le pli, sauf si une championne (dame) est jouée. Si une championne est jouée, cette dernière remporte le pli. même s’il n’est pas de la couleur demandée.
- 4 Atout : Doppleganger. Est une copie de la carte jouée par le joueur précédent mais d’une valeur supérieure. Si le Doppleganger est la première carte jouée dans un pli, il copie la dernière carte jouée du pli précpdent.
- 5 Atout : Momie. Est une copie de la carte qui a remporté le pli précédent, de même couleur et de même valeur. Ne peut pas être jouée lors du premier pli d’une donne.

- 1 pique : Bouffon +3 points si vous arrivez à jouer cette carte lors du dernier pli
- 1 carreau : Ninja. Vous marquez un point pour chaque figure (vdra) jouée dans le même pli que cette carte
- 1 trèfle : druide. Après ce pli, échangez votre main avec celle du joueur de votre choix
- 11 carreau porte étendard : révélez cette carte après que la quête a été choisie, avant le premier pli. Piochez au hasard deux carte dans la main du joueur de votr echoix puis donnez lui deux cartes de votre choix.
- 11 carreau Shaman : -3 points pour le jouer qui remporte cette carte
- 11 trefle : bricoleur. Le vainqueur du prochain pli marque 3 points. Cette carte est sans effet si elle est jouée au dernier pli
- 11 trèfle porte étendard : +3 points pour le joueur qui remporte cette carte
- 11 pique enchanteur : le joueur qui remporte cette carte double les points positifs ou négatifs gagnés lors de cette donne
- 11 pique porte étendard : Après ce pli, chaque joueur donne les cartes de sa main à son voisin de droite ou de gauche selon votre choix


### Les 5
- 5 trèfle : celui qui la remporte distribue la prochaine donne. Il pioche la carte spéciale.
- 5 pique : le joueur qui l’a en main pioche la carte quête et choisi la règle de score pour cette donne.
- 4 carreau : le joueur qui l’a en main commence la partie

### Cartes quête
- 8,9 trèfle : +1 points pour chaque carte trèfle, 8 et 9 (les 8 et 9 de trèfle remportent donc 2 points).
  ou -1 pour chaque
- +5 points si vous remportez exactement trois plis
  ou +5 points si vous remportez exactement un pli.
- Marquez autant de points que la valeur de votre plus faible carte (10 pour les têtes)
  ou perdez autant de points que la valeur le votre plus faible carte (rien pour les têtes)
- Marquez autant de points que le nombre de cartes dans la couleur où vous en avez le moins (peut être zéro)
  ou Marquez autant de points que le nombre de cartes dans la couleur où vous en avez le plus
- Marquez autant de points que la valeur de votre plus petit trèfle. Rien pour les têtes.
  ou Marquez autant de points que la valeur de votre plus petit pique. Rien pour les têtes.
- +3 par roi et -3 par reine
  ou -3 par roi et +3 par reine
- +1 par pique
  ou +1 par carreaux
- +5 si aucun autre joueur ne remporte le même nombre de plis que vous
  ou +5 si au moins un joueur remporte le même nombre de plis que vous.
- -1 par pique
  ou -1 par carreaux
- +5 si vous avez un nombre paire de pli (sauf 0)
  ou +5 si vous avez un nombre impaire de pli
- +4 points par carte 3 et 4
  ou +4 points par cartes 6 et 7
- +5 points si vous remportez exactement 4 plis
  ou +5 si vous remportez exactement 2 plis
- +1 par pli remporté par votre voisin de droite
  ou +1 par pli remporté par votre voisin de gauche
- +4 points par valets
  ou -4 points par valets
- +1 par pique et -1 par trèfle
  ou -1 par pique et +1 par trèfle
- +1 pour chaque tête et chaque pique (+2 pour une tête de pique)
  ou -1 pour chaque tête et chaque pique (-2 pour une tête de pique)
- +1points par brelan (3 cartes de la même valeur). +2 points par brelans de figures. 
  ou marquez autant de points que le nombre de cartes dans votre plus longue suite (sans tenir compte de la couleur) d’au moins trois cartes.
- +8 pour le roi de trèfle
  ou -8 pour le roi de trèfle
- +2 pour chacun des quatre derniers plis
  ou -2 pour chacun des quatre derniers plis
- +1 par pli
  ou -1 par pli





## Préparation
- 3 joueurs : Le 2 de trèfle est retiré définitivement du jeu : 38 cartes
- 4 ou 5 joueurs : 39 cartes
On ajoute à ces cartes une carte spéciale que l’on pioche au hazard avant de distribuer.
Celle du tour précédent est définitivement défaussée.

