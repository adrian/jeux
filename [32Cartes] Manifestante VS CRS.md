# La manifestante VS La CRS

* 2 joueuses
* 20 minutes
* à partir de 10 ans (c'est ce qu'il y a sur la boîte, ça vaut ce que ça vaut)

Un.e joueuse incarne une manifestante qui veut maintenir le rapport de force à égalité (pour que la manif avance) et une CRS qui cherche à le faire basculer d'un côté ou de l'autre (pour casser des têtes ou pleurer sur BFMtv selon la situation).

Les joueuses vont devoir remporter et perdre des plis pour influencer l'opinion publique (coeur), attaquer le camp adverse (pique) ou s'en défendre (carreau) et renverser des situation grâce à la médecine par les plantes (Trèfle) (street medic, DIY pepper spray...).

> Très librement adapté de *Jekyll vs Hyde*
>
> Le lore original étant psychophobe nous l'avons complètement réécrit. Libre à vous de recommencer s'il ne vous plaît pas !
>
> Note d'adaptation :
>
> * Orgueil -> carreau
> * Colère -> pique
> * Envie -> coeur
> * Potion -> trefle
> * Jekyll -> Manifestante
> * Hyde -> CRS

## Matériel

* 7 cartes (de 1 à 7 ou 7 à dame) en cœur, pique et carreau
* 4 cartes ( de 1 à 4) en trèfle
* 3 cartes (des dames ou des as par exemple) de chaque couleurs (cœur, pique et carreau) pour faire des marqueurs
* De quoi compter un score (jusqu'à 10) : papier+stylo, jetons, tonfas volées en manif...

## Aperçu

La partie se joue en trois manches. à l'issue de chaque manche l'équilibre de la manif se dégrade sous les assauts des CRS...

## Mise en place

1. Déterminer qui sera la CRS et qui sera la manifestante
2. Initialiser le score de la CRS à 0
3. Placer les trois cartes marqueur à côté du plateau
4. Mélanger les cartes de jeu

## Déroulement d'une manche

### 1\. Préparation

Chaque joueuse reçoit 10 cartes (5 sont mises de côté)

Chaque joueuse choisit une carte de sa main et l'échange avec son adversaire (échange caché et simultané). On échange deux cartes au début de la deuxième manche et trois à la troisième.  
*Si vous recevez au moins 2 cartes trèfle vous devez en inclure une dans l'échange*

### 2\. Lutte

Si la CRS a 5 points ou plus elle commence, sinon c'est la manifestante qui commence la manche.

Une manche se joue en 10 plis, qui se déroulent chacun comme suit :  
• la première joueuse joue n’importe quelle carte de sa main, face visible.  
• si la première joueuse joue une carte d’une couleur donnée, son adversaire doit jouer une carte de cette couleur. Elle peut aussi jouer une carte Trèfle à la place. Si elle n’a pas la couleur demandée, elle peut jouer n’importe quelle carte de sa main.  
• si la première joueuse joue une carte Trèfle, alors elle doit annoncer une couleur. Son adversaire doit jouer cette couleur (même si elle a une carte Trèfle !). Si elle ne peut pas, elle peut jouer n’importe quelle carte de sa main.  
On compare ensuite les cartes :  
• si les deux cartes sont de la même couleur, la carte de plus haute valeur l’emporte.  
• si les deux cartes sont de couleur différente, la couleur la plus forte l’emporte (voir plus loin), et ce quelle que soit sa valeur.  
• si un Trèfle a été jouée, résolvez d’abord son effet ; puis, la carte de plus haute valeur l’emporte. La vainqueuse du pli ramasse les deux cartes et devient la première joueuse du pli suivant.

> #### Ordre des couleurs
>
> Les couleurs ne sont pas classées au début de la partie. C’est en cours de manche que leur rang sera défini.  
> La première couleur à apparaître (première carte jouée) acquiert automatiquement le rang le plus faible. Placez immédiatement le marqueur de la couleur correspondante.  
> Lorsque la deuxième couleur apparaît, placez son jeton sur la case du milieu. Dès cet instant, la couleur qui n’a pas encore été jouée devient automatiquement la meilleure couleur de la manche.  
> Les cartes Trèfle n’ont pas de couleur et ne peuvent pas être classées

> #### Cartes trèfle
>
> Lorsqu’une joueuse joue une carte Trèfle, son effet dépend de la carte jouée par son adversaire :  
> **Pique** : la vainqueuse de ce pli vole un pli (composé de 2 cartes) à son adversaire.  
> **Carreau** : les deux joueuses choisissent 2 cartes de leur main (ou 1, si c’est la dernière) et les échangent simultanément.  
> **Cœur** : retirez les marqueurs du plateau. L’ordre des couleurs est réinitialisé : la prochaine carte jouée définira la couleur la plus faible, et ainsi de suite.
>
> Dans tous les cas, lorsqu’une carte trèfle est jouée, c’est toujours la plus haute valeur qui l’emporte.  
> Double trèfle : si les deux joueurs posent une carte trèfle, alors elles se neutralisent. Il ne se passe rien et la plus haute valeur l’emporte.  
> **Note : les cartes trèfle remportent les égalités.**

### 3\. Progression de la manif

Comparez le nombre de plis remporté par chaque joueuse durant cette manche puis soustrayez le score le plus faible du score le plus élevé (par exemple si la CRS a gagné 6 plis contre 4, le résultat est de 6 - 4 =2). Ce résultat doit être ajouté au score de la CRS.  
Le déséquilibre est inexorable, le score ne peut pas diminuer et même si la manifestante gagne plus de plis que la CRS, cette dernière gagnera des points ! L'objectif de la manifestante est donc l'équilibre des plis.

## Fin de la partie

La CRS gagne si elle réussi a totaliser un score de 10 points avant ou à la fin des trois manches. Sinon la manifestante gagne.

## VARIANTE : JOUER EN ALLER/RETOUR

Jouer la manifestante est délicat : vous serez constamment sous pression et planifierez soigneusement toute action. Pour une partie plus équilibrée et plus tactique, jouez deux parties en alternant les rôles. Après vos deux parties, la meilleure CRS gagne le match. Si les deux CRS ont marqué 10 points chacune, celle qui y est parvenu le plus rapidement l’emporte. S’il y a encore égalité, la partie se termine sur cette égalité.