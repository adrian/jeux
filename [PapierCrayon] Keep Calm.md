# Keep Calm

* 3 joueureuses
* 10 à 15 minutes par tour (ou plus si les gens sont forts), autant de tours que vous voulez

## Matériel

* Un grand papier (A4) et un stylo 
* Un sablier

## Préparation

* On désigne lae joueureuse principale
* Une personne se met à sa gauche, une autre à sa droite
* On met un papier et un crayon dans la main principale de lea joueureuse principale
* On retourne le sablier

## Déroulement d'un tour

* Chaque personne lui posera une question à la fois.
  *Les deux peuvent parler en même temps pour plus de difficulté*
* Il faut répondre à l’écrit pour la question posée du côté du papier et à l’oral pour l’autre côté
  *On peut inverser pour plus de challenge*
* Lae joueureuses principale doit retourner le sablier sans jamais le regarder.
  *Iel peut se contenter de dire « retourner » et les autres personnes s’en chargent*

## Fin du tour
Quand le sablier est vide le tour s’arrête, le nombre de questions répondues (correctement) peut constituer un score.

